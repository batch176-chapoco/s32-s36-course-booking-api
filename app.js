//[SECTION] Dependencies and Modules
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const dotenv = require('dotenv');
const userRoutes = require('./routes/users') //routing system users
const courseRoutes = require('./routes/courses')

//[SECTION] Environment Setup
dotenv.config();
let account = process.env.CREDENTIALS;
const port = process.env.PORT;

//[SECTION] Server Setup
const app = express();
app.use(express.json());////the app should be able to recognize data written in a json format
app.use(cors());//it enables all origins/address/URL of the client request

let corsOptions = {
    origin: ['http://localhost:3000', 'http://example.com']
}
//[SECTION] Database Connection
mongoose.connect(account)
const connectStatus = mongoose.connection
connectStatus.once('open', () => console.log(`Database Connected`));

//[SECTION] Backend Routes
//http://localhost:4000/users  
app.use('/users', userRoutes);
//http://localhost:4000/courses
app.use('/courses', courseRoutes);
//[SECTION] Server Gateway Response
app.get('/', (req, res) => {
    res.send('Welcome to the Enrollment-System')
});

app.listen(port, () => {
    console.log(`API is Hosted port ${port}`)
});
