//[SECTION] Dependencies and Modules
const mongoose = require('mongoose')
//[SECTION] Schema/Blueprint
const courseSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: [true, 'Course Price is Required']
    },
    isActive: {
        type: Boolean,
        default: true
    }, //this is to identify if the subject is currently being offered by the institution.
    createdOn: {
        type: Date,
        default: new Date()
    }, //'new Date()' expression instantiates a new date that stores the current date and time whenever a course is created in the database.
    enrollees: [
        {
            //each object stored in this array would identify a single student.
            userId: {
                type: String,
                required: [true, 'Student ID is Required']
            },
            enrolled: {
                type: Date,
                default: new Date()
            }
        }
    ]
})
//[SECTION] Model
module.exports = mongoose.model('Course', courseSchema)