//[SECTION] Modules
const express = require('express')
const mongoose = require('mongoose')
const { required } = require('nodemon/lib/config')

//[SECTION] Schema/Blueprint
const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'First Name is Required']
    },
    lastName: {
        type: String,
        required: [true, 'Last Name is Required']
    },
    email: {
        type: String,
        required: [true, 'Email is Required']
    },
    password: {
        type: String,
        required: [true, 'Password is Required']
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    mobileNo: {
        type: String,
        required: [true, 'Mobile Number is Required']
    },
    enrollments: [
        {
            courseId: {
                type: String,
                required: [true, 'Subject ID is Required']
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            },
            status: {
                type: String,
                default: 'Enrolled'
            }
        }
    ]

})

//[SECTION] Model
module.exports = mongoose.model('User', userSchema);